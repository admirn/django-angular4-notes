import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { User } from '../_models/user';

@Injectable()
export class UserService {
  user: User;
  constructor(
    private http: Http,
  ) { }
  
  
  
  login(fromValue): Observable<boolean> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({ fromValue });
    const urlLogin = 'http://localhost:8000/rest-auth/login/';
    return this.http.post(urlLogin, fromValue, options)
      .map(
        (response: Response) => {
          if(response.json() && response.json().token){
            localStorage.setItem('token', JSON.stringify({ token: response.json().token }));
            localStorage.setItem('user', JSON.stringify({ user: response.json().user }));
          }return response.json();
        }
      ).catch((error: any) => Observable.throw(error || 'server error'));
  }
  
  registration(fromValue): Observable<boolean> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let username = fromValue.username;
    let password1 = fromValue.password1;
    let password2 = fromValue.password2;
    let email = fromValue.email;
    let body = JSON.stringify({ username: username, password1: password1, password2: password2, email: email,  });
    const url = 'http://localhost:8000/registration/';
    return this.http.post(url, body, options)
      .map(
        (response: Response) => {
          if(response.json()){
            
          }return response.json();
        }
      );
  }
  
  resetPassword(fromValue): Observable<boolean> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let email = fromValue.email;
    let body = JSON.stringify({ email: email });
    const url = 'http://localhost:8000/rest-auth/password/reset/';
    return this.http.post(url, body, options)
      .map(
        (response: Response) => {
          if(response.json()){
            
          }return response.json();
        }
      );
  }
  
  verify(fromValue): Observable<boolean> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let key = fromValue.key;
    let body = JSON.stringify({ key: key });
    const url = 'http://localhost:8000/registration/verify-email/';
    return this.http.post(url, body, options)
      .map(
        (response: Response) => {
          if(response.json()){
            
          }return response.json();
        }
      );
  }
  
  logout(): void{
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }
  
  get authenticated(){
    if(localStorage.getItem('user') && localStorage.getItem('token')){
      return true;
    }else{
      return false;
    }
  }
  
}
