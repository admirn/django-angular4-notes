import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { Note } from '../_models/note';

@Injectable()
export class NoteService {
  note: Note;

  constructor(
    private http: Http,
  ) { }
  
  getN(): Observable<boolean>{
    const localToken = localStorage.getItem('token');
    const localT = JSON.parse(localToken);
    const urlNotes = 'http://localhost:8000/notes/';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization','Token ' + localT['token']);
    let options = new RequestOptions({ headers: headers });
    return this.http.get(urlNotes, options)
      .map(
        (response:Response) => {
          return response.json();
        }
      ).catch((error: any) => Observable.throw(error || 'server error'));
  }
  
  createN(formValue): Observable<boolean>{
    const localToken = localStorage.getItem('token');
    const localT = JSON.parse(localToken);
    const urlNotes = 'http://localhost:8000/notes/';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization','Token ' + localT['token']);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(urlNotes, formValue, options)
      .map(
      (response:Response) => {
        return response.json();
      }
    ).catch((error: any) => Observable.throw(error || 'server error'));
  }
  updateN(formValue, id): Observable<boolean>{
    const localToken = localStorage.getItem('token');
    const localT = JSON.parse(localToken);
    const urlNotes = 'http://localhost:8000/notes/';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization','Token ' + localT['token']);
    let options = new RequestOptions({ headers: headers });
    return this.http.put(urlNotes+id+'/', formValue, options)
      .map(
      (response:Response) => {
        return response.json();
      }
    ).catch((error: any) => Observable.throw(error || 'server error'));
  }
  
  deleteN(id: number): Observable<boolean>{
    const localToken = localStorage.getItem('token');
    const localT = JSON.parse(localToken);
    const urlNotes = 'http://localhost:8000/notes/';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization','Token ' + localT['token']);
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(urlNotes+id+'/', options)
      .map(
        (response:Response) => {
          return response.json();
        }
      ).catch((error: any) => Observable.throw(error || 'server error'));
  }
  
  getNote(id: number): Observable<Note>{
    const localToken = localStorage.getItem('token');
    const localT = JSON.parse(localToken);
    const urlNotes = 'http://localhost:8000/notes/';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization','Token ' + localT['token']);
    let options = new RequestOptions({ headers: headers });
    return this.http.get(urlNotes + id + '/', options)
      .map(
        (response:Response) => {
          return response.json();
        }
      ).catch((error: any) => Observable.throw(error || 'server error'));
  }
  
  loadMoreN(created): Observable<Note>{
    const localToken = localStorage.getItem('token');
    const localT = JSON.parse(localToken);
    const urlNotes = 'http://localhost:8000/notemore/';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization','Token ' + localT['token']);
    let options = new RequestOptions({ headers: headers });
    return this.http.get(urlNotes + created + '/', options)
      .map(
        (response:Response) => {
          return response.json();
        }
      ).catch((error: any) => Observable.throw(error || 'server error'));
  }
  
  searchN(input: string): Observable<Note>{
    const localToken = localStorage.getItem('token');
    const localT = JSON.parse(localToken);
    const urlNotes = 'http://localhost:8000/notes/search?title=';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization','Token ' + localT['token']);
    let options = new RequestOptions({ headers: headers });
    return this.http.get(urlNotes + input, options)
      .map(
        (response: Response) => {
          return response.json();
        }
      ).catch((error: any) => Observable.throw(error || 'server error'));
  }

}
