import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { PasswordResetComponent } from './user/password-reset/password-reset.component';
import { PasswordResetCompleteComponent } from './user/password-reset-complete/password-reset-complete.component';
import { VerifyEmailComponent } from './user/verify-email/verify-email.component';

import { HomeComponent } from './home/home/home.component';

import { NoteCreateComponent } from './home/note/note-create/note-create.component';
import { NoteDetailComponent } from './home/note/note-detail/note-detail.component';
import { NoteEditComponent } from './home/note/note-edit/note-edit.component';
//guard
import { AuthGuard } from './_guards/auth.guard';
import { LoginGuard } from './_guards/login.guard';

     
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',  component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'registration', component: RegistrationComponent, canActivate: [LoginGuard] },
  { path: 'account-confirm-email/:key', component: VerifyEmailComponent, canActivate: [LoginGuard] },
  { path: 'password-reset', component: PasswordResetComponent, canActivate: [LoginGuard] },
  { path: 'password/reset/key/:key/:token', component: PasswordResetCompleteComponent, canActivate: [LoginGuard] },
  
  { path: 'note/create', component: NoteCreateComponent, canActivate: [AuthGuard] },
  { path: 'note/:id/detail', component: NoteDetailComponent, canActivate: [AuthGuard] },
  { path: 'note/:id/edit', component: NoteEditComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes), ],
  exports: [ RouterModule, ]
})
export class AppRoutingModule {}
