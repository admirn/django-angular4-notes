import { Component, OnInit } from '@angular/core';
//for forms
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../_services/user.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css'],
  providers: [UserService]
})
export class PasswordResetComponent implements OnInit {
  formReset: FormGroup;
  error: string = null;
  success: string = null
  allMessages = [];
  allMessages2 = [];

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
  ) { }
  
  createForm(){
    this.formReset = this.formBuilder.group({
      email:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
    });
  }
  
  onSubmit(){
    this.userService.resetPassword(this.formReset.value)
      .subscribe(
        result => {
          this.success = result['detail'];
          this.allMessages = [];
          this.allMessages2 = [];
          this.formReset.reset();
        },
        error => {
          this.allMessages = JSON.parse(error['_body']);
          console.log(this.allMessages['email']);
          this.allMessages2 = this.allMessages['email'];
          console.log(this.allMessages2);
          if(Object.getPrototypeOf(this.allMessages['email']) === Object.prototype){
            this.allMessages = [];
          }
          this.success = null;
          this.formReset.reset();
        }
      );
  }

  ngOnInit() {
    this.createForm();
  }

}
