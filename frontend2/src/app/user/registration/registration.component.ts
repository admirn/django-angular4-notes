import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../_services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [UserService]
})
export class RegistrationComponent implements OnInit {
  formRegistration: FormGroup;
  success: string = null;
  allMessages = [];
  

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
  ) { }
  
  createForm(){
    this.formRegistration = this.formBuilder.group({
      username:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
      email:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
      password1:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
      password2:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160), Validators]],
    });
  }
  
  onSubmit(){
    this.userService.registration(this.formRegistration.value)
      .subscribe(
        result => {
          this.success = result['detail'];
          this.formRegistration.reset();
          this.allMessages = [];
        },
        error => {
          this.allMessages = JSON.parse(error['_body']);
          console.log(this.allMessages);
        }
      );
  }

  ngOnInit() {
    this.createForm();
  }

}
