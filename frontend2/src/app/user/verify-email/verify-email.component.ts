import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../_services/user.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css'],
  providers: [UserService]
})
export class VerifyEmailComponent implements OnInit {
  key: string = null;
  formConfirm: FormGroup;
  success: string = null;
  error: string = null;
  login: boolean = false;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  getKey(){
    this.activatedRoute.params.subscribe((params: Params) => {
      this.key = params['key'];
    });
  }
  
  createForm(){
    this.formConfirm = this.formBuilder.group({
      key:[this.key, [Validators.required]],
    });
  }
  
  onSubmit(){
    this.userService.verify(this.formConfirm.value)
      .subscribe(
        result => {
          this.success = 'E-mail has been activated';
          this.error = null;
          this.login = true;
          this.formConfirm.reset();
        },
        error => {
          this.success = null;
          console.log(error);
          let message = JSON.parse(error['_body']);
          this.error = message['detail'];
        }
      );
  }

  ngOnInit() {
    this.getKey();
    this.createForm();
  }

}
