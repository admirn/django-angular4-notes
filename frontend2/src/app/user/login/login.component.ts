import { Component, OnInit } from '@angular/core';
//for forms
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../_services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  // user: User;
  error: string = null;
  allMessages = [];

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
  ) { }
  
  createForm(){
    this.formLogin = this.formBuilder.group({
      username:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
      password:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
    });
  }
  onSubmit(){
    this.userService.login(this.formLogin.value)
      .subscribe(
        result => {
          this.router.navigate(['/home']);
        },
        error => {
          this.allMessages = JSON.parse(error['_body']);
          console.log(this.allMessages);
          this.formLogin.reset();
        }
      );
  }
  

  ngOnInit() {
    this.createForm();
  }

}
