import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';

//for routing
import { AppRoutingModule }     from './routing.module';
//for forms
import { ReactiveFormsModule } from '@angular/forms';

import { PasswordResetComponent } from './user/password-reset/password-reset.component';
import { NavigationComponent } from './home/navigation/navigation.component';
import { ContentComponent } from './home/content/content.component';
import { HomeComponent } from './home/home/home.component';
//guard
import { AuthGuard } from './_guards/auth.guard';
import { LoginGuard } from './_guards/login.guard';
import { PasswordResetCompleteComponent } from './user/password-reset-complete/password-reset-complete.component';
import { VerifyEmailComponent } from './user/verify-email/verify-email.component';

import { NoteCreateComponent } from './home/note/note-create/note-create.component';
import { NoteDetailComponent } from './home/note/note-detail/note-detail.component';
import { NoteEditComponent } from './home/note/note-edit/note-edit.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    NavigationComponent,
    PasswordResetComponent,
    NavigationComponent,
    ContentComponent,
    HomeComponent,
    PasswordResetCompleteComponent,
    VerifyEmailComponent,
    NoteCreateComponent,
    NoteDetailComponent,
    NoteEditComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [
    AuthGuard,
    LoginGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
