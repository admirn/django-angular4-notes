import { Component, OnInit } from '@angular/core';
import { NoteService } from '../../_services/note.service';
import { Note } from '../../_models/note';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
  providers: [NoteService]
})
export class ContentComponent implements OnInit {
  notes: Note[];
  selectedNote: Note;
  currentDate = new Date();
  successDelete: string = null;
  loadMoreButton: boolean = false;
  lastItem = null;

  constructor(
    private noteService: NoteService,
  ) { }
  
  getNotes(){
    this.noteService.getN()
      .subscribe(
        result => {
          this.notes = result['results'];
          if(result['count'] === 8){
            this.loadMoreButton = true;
          }
          this.lastItem = this.notes[this.notes.length - 1];
        },
        error => {
          console.log(error);
        }
      );
  }
  
  delete(note: Note){
    this.noteService.deleteN(note.id)
      .subscribe(
        result =>{
          this.notes = this.notes.filter(h => h !== note);
          if (this.selectedNote === note){ 
            this.selectedNote = null; 
          }
          this.successDelete = 'Successfully deleted note!';
          setTimeout(() => {
            this.successDelete = null;
          },3000);
        },
        error => {
          console.log(error);
        }
      )
  }
  
  loadMore(item){
    this.noteService.loadMoreN(item['created'])
      .subscribe(
        result => {
          for(let key in result){
            if(result.hasOwnProperty(key)){
              this.notes.push(result[key]);
            }
          }
          this.lastItem = this.notes[this.notes.length - 1];
          if(result[3] == null){
            this.lastItem = null;
            this.loadMoreButton = false;
          }
        },
        error => {
          console.log(error);
        }
      );
  }
  
  onSelect(note: Note){
    this.selectedNote = note;
  }
  deselectHero(){
    this.selectedNote = null;
  }

  ngOnInit() {
    this.getNotes();
  }

}
