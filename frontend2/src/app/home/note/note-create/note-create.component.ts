import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { NoteService } from '../../../_services/note.service';

@Component({
  selector: 'app-note-create',
  templateUrl: './note-create.component.html',
  styleUrls: ['./note-create.component.css'],
  providers: [NoteService]
})
export class NoteCreateComponent implements OnInit {
  formCreate: FormGroup;
  successMessage:string = null;
  
  constructor(
    private formBuilder: FormBuilder,
    private noteService: NoteService,
    private router: Router,
  ) { }
  
  createForm(){
    this.formCreate = this.formBuilder.group({
      title:['', [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
      body:['', [Validators.required, Validators.minLength(10), Validators.maxLength(1600)]],
    });
  }
  
  onSubmit(){
    this.noteService.createN(this.formCreate.value)
      .subscribe(
        result => {
          this.formCreate.reset();
          this.successMessage = 'Note created successfully! You will be redirected...';
          setTimeout(() => {
            this.successMessage = null;
            let link = ['/note/'+result['id']+'/detail/'];
            this.router.navigate(link);
          },3000);
        },
        error => {
          console.log(error);
          this.formCreate.reset();
        }
      );
  }

  ngOnInit() {
    this.createForm();
  }

}
