import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { Note } from '../../../_models/note';
import { NoteService } from '../../../_services/note.service';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.css'],
  providers: [NoteService]
})
export class NoteDetailComponent implements OnInit {
  note: Note;
  constructor(
    private noteService: NoteService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
  ) { }
  
  loadNote(){
    this.route.params
      .switchMap((params: Params) => this.noteService.getNote(+params['id']))
      .subscribe(
        result => {
          this.note = result;
        },
        error => {
          // console.log(error);
          let link = ['/home'];
          this.router.navigate(link);
        }
      );
  }
  
  goBack(){
    this.location.back();
  }

  ngOnInit() {
    this.loadNote();
  }

}
