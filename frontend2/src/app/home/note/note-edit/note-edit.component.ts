import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { NoteService } from '../../../_services/note.service';
import { Note } from '../../../_models/note';

@Component({
  selector: 'app-note-edit',
  templateUrl: './note-edit.component.html',
  styleUrls: ['./note-edit.component.css'],
  providers: [NoteService],
})
export class NoteEditComponent implements OnInit {
  note: Note;
  formUpdate: FormGroup;
  successMessage:string = null;
  title: string = null;
  body: string = null;
  
  constructor(
    private formBuilder: FormBuilder,
    private noteService: NoteService,
    private route: ActivatedRoute,
    private router: Router
  ) { }
  
  createForm(){
    this.formUpdate = this.formBuilder.group({
      title:[this.title, [Validators.required, Validators.minLength(3), Validators.maxLength(160)]],
      body:[this.body, [Validators.required, Validators.minLength(10), Validators.maxLength(1600)]],
    });
  }
  
  loadNote(){
    this.route.params
      .switchMap((params: Params) => this.noteService.getNote(+params['id']))
      .subscribe(
        result => {
          this.title = result['title'];
          this.body = result['body'];
          this.note = result;
          this.createForm();
        },
        error => {
          // console.log(error);
          let link = ['/home'];
          this.router.navigate(link);
        }
      );
  }
  onSubmit(id: number){
    this.noteService.updateN(this.formUpdate.value, id)
      .subscribe(
        result => {
          this.formUpdate.reset();
          this.successMessage = 'Note edited successfully! You will be redirected...';
          setTimeout(() => {
            this.successMessage = null;
            let link = ['/note/'+result['id']+'/detail/'];
            this.router.navigate(link);
          },3000);
        },
        error => {
          console.log(error);
          this.formUpdate.reset();
        }
      );
  }

  ngOnInit() {
    this.loadNote();
  }

}
