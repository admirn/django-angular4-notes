import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { NoteService } from '../../_services/note.service';
import { Router } from '@angular/router';
import { User } from '../../_models/user';
import { Note } from '../../_models/note';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  providers: [UserService, NoteService]
})
export class NavigationComponent implements OnInit {
  user: User;
  notes: Note;

  constructor(
    private userService: UserService,
    private noteService: NoteService,
    private router: Router
  ) { }
  
  logout(){
    this.userService.logout();
    this.user = null;
    this.router.navigate(['/login']);
  }
  
  getUser(){
    if(JSON.parse(localStorage.getItem('user'))){
      let puser = JSON.parse(localStorage.getItem('user'));
      this.user = puser['user'];
    }return this.user;
  }
  
  searchNote(input: string){
    if(input == ''){
      this.notes = null;
    }else{
      setTimeout(()=>{
        this.noteService.searchN(input)
          .subscribe(
            result => {
              this.notes = result['results'];
            },
            error => {
              console.log(error);
            }
          );
      }, 500);
    }
  }
  
  ngOnInit() {
    this.getUser();
  }

}
