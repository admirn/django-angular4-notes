import { Frontend2Page } from './app.po';

describe('frontend2 App', () => {
  let page: Frontend2Page;

  beforeEach(() => {
    page = new Frontend2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
