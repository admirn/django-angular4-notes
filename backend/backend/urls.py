from django.conf.urls import url, include
from django.contrib import admin
from allauth.account.views import password_reset, email_verification_sent, password_reset_from_key, confirm_email

urlpatterns = [
    url(r'^', include('notes.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^registration/', include('rest_auth.registration.urls')),
    #url('^', include('allauth.urls')),
    #url(r"^account-confirm-email/(?P<key>[-:\w]+)/$", confirm_email, name="account_confirm_email"),
    #without abouve it goes to rest_auth.registration.urls
    url(r"^account-confirm-email/$", email_verification_sent, name="account_email_verification_sent"),
    #url(r"^password/reset/$", password_reset, name="account_reset_password"),
    url(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)/(?P<key>.+)/$", password_reset_from_key, name="account_reset_password_from_key"),
]
