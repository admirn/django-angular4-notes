from .models import Note
from .serializers import NoteSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions

from .permissions import IsOwner

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework import status

from rest_framework.renderers import JSONRenderer

import json
from django.http import JsonResponse
from rest_framework.decorators import api_view

class NoteList(generics.ListCreateAPIView):
    #queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = (permissions.IsAuthenticated,)
    
    def get_queryset(self):
        user = self.request.user
        return Note.objects.filter(owner=user)[:8] 
        
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class NoteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = (IsOwner, )

  
class PurchaseList(generics.ListAPIView):
    serializer_class = NoteSerializer
    permission_classes = (permissions.IsAuthenticated, )  
    
    def get_queryset(self):
        user = self.request.user
        queryset = Note.objects.filter(owner=user) 
        # queryset = Note.objects.all()
        title = self.request.query_params.get('title', None)
        if title is not None:
            queryset = queryset.filter(title__contains=title)[:5]
        return queryset    

                
class NoteLoadMore(APIView):    
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = NoteSerializer
    #renderer_classes = (JSONRenderer, )        
    def get(self, request, date, format=None):
        user = self.request.user
        notes = Note.objects.filter(owner=user).filter(created__lt=date)[:4]
        serializer = NoteSerializer(notes, many=True)
        return Response(serializer.data)
        # return Response(JSONRenderer().render(serializer.data))

