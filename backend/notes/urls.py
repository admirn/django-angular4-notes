from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^notes/$', views.NoteList.as_view()),
    url(r'^notes/(?P<pk>[0-9]+)/$', views.NoteDetail.as_view()),
    url(r'^notes/search$', views.PurchaseList.as_view()),
    url(r'^notemore/(?P<date>.*)/$', views.NoteLoadMore.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)