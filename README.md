# Django Notes app with Django 1.10 and Angular 4

This is a simple Notes app with Django as backend and Angular4 as frontend

## Installing backend (works with python2.7)
```
git clone https://admirn@bitbucket.org/admirn/django-angular4-notes.git
```
```
cd django-angular4-notes
```
```
virtualenv env
```
```
source ./env/bin/activate
```
```
pip install -r requirements.txt
```
```
cd backend
```
```
python manage.py makemigrations
```
```
python manage.py migrate
```
```
python manage.py runserver
```
Needs SMTP configuration for sending emails

## Installing frontend (works with Angular/Cli 1.20)
If you don't have installed Angular then install (requires NodeJS > 4):
```
npm install --save-dev @angular/cli@1.2.0
```
If you have installed another Cli then remove it by:
```
npm uninstall --save-dev angular-cli
```
```
npm cache clean
```
and run the installation command for Cli 1.2.0

Go into the frontend2 folder
```
cd fontend2
```
Run the npm install
```
npm install
```
After the installation is complete run:
```
ng serve
```
IMPORTANT! Both, backend and frontend servers must run at the same time.

